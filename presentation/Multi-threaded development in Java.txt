@page title@ ​Introduction 
    •Overview of concurrent and co-operative multithreading 
    •Purpose: 
        –Draw attention to problems 
        –Provide techniques to avoid them 
        –Mention some more advanced services so you know they are available 

​@page title@ Before we begin: Types of parallel activities 
    ​Motivations for parallelism: 
        –Better use of resources 
            •Exploit multiple processing units 
            •Perform work on the CPU while other activities require I/O
            or are blocked (waiting for something) [this even works in
            the single-CPU case!] 
        –Natural modelling of requirements 
            •Handling concurrent requests of several clients 
            •Perform background activities, scheduled tasks, provide
            periodic updates 

@page title@ ​Before we begin: Types of parallel processing 
    ​Types of parallel processing: 
        –Multiprocessing 
            •Several processes 
            •Maybe on different computers 
            •IPC (inter-process communication) via network/OS/shared
            memory/messaging 
        –Multithreading 
            •Within the same process 
            •Shared heap, private stack 
@page title@ ​The Java Memory Model 
    •Need to properly synchronise access to shared data. 
    •Proper synchronisation means establishing an ordering of actions to
    guarantee visibility(it is not simply about mutual exclusion, and
    not simply about the synchronizedkeyword!). 

@page title@ ​Synchronisation – why is it needed? 
    •Compilers may reorder code 
    •Values may be cached in private storage (CPU cache or registers) 
        –writing a variable may not be committed to shared memory 
        –if cached value is already available, updated value may not be
        read from shared memory

@page title@ Synchronisation - effects
    •Synchronisation forces: 
        –flushing updated private copies to shared memory 
        –updating private copy from shared memory 

@page title@ ​Reordering, speculative execution
@@@ several pages with images of example @@@

@page title@ ​Ordering: the ‘Happens-before’ relation 
  •Within a thread: actions will appear to follow the order of
    statements in the source (e.g. a read of a local variable after a
    write will work as expected: the write happens-beforethe read). This
    is called ‘program order’. Note: statements may be reordered by the
    compiler/hardware, but the outcome will be the as dictated by the
    source. 
    •synchronized: Leaving a synchronized block happens-before every
    subsequent entry into blocks synchronised on the same object. 
    •volatile:  A write to a volatile field happens-before every
    subsequent read of that same field. 
    •Thread.start():  A call to start() on a thread
    happens-before anything in the started thread. 
    •Thread termination: Everything in a thread
    happens-before join()returns successfully, or Thread.isAlive()returns
    false. 
    •Interruption: A thread calling interrupt()on another thread
    happens-beforethe interrupted thread detects the interrupt
    (InterruptedExceptionis thrown, or  isInterrupted()or
    interrupted()return true). 
    •finalize():  The end of an object’s constructor happens-beforethe
    first statement of its finalize(). 
    •Transitivity: if A happens-beforeB, and B happens-beforeC, then A
    happens-beforeC. 

@page title@ ​Unordered operations – hidden surprises 
    •The is absolutely no guarantee one operation will affect another
    unless they are properly synchronised. This is not just theory –
    we’ll soon see it in action. 
    •The surprise may only come on the server – server VM uses more
    agressive optimisations. 
    •‘With the exception of 32-bit Windows, the server VM will
    automatically be selected on server-class machines’ 

@page title@ ​Overview of basic parallelism support 
    •Basic types: 
        –Runnable 
        –Thread 
    •Keywords: 
        –synchronized 
        –volatile 
        –final 

@page title@ java.lang.Runnable 
    •Interface 
    •Work target for a Thread (requires a thread to execute) 
    •Single method: 
        public void run(); 

@page title@ ​java.lang.Thread 
    ●Class 
    ●Implements Runnable 
    ●May be subclassed (extended), or parameterised with a Runnable 
    ●Never call its deprecated methods!
    
@page title@ Thread vs Runnable
    ●It’s better to implement Runnable than to extend Thread 
        ●you only have one shot at inheritance 
        ●you don’t have to be careful not to override Thread’s methods
        (e.g. you can have a stop() method, which is normally provided
        by Thread (and is deprecated)). 

@page title@ Life cycle of a Thread 
@@@ image @@@

@page title@ Thread's scheduling states
@@@ image @@@

@page title@ ​java.lang.Thread API 
    - start(): starts the Thread 
    ​- run(): main method, Thread dies when exits (inc. exceptions);
    cannot be restarted 
    ​- interrupt(), isInterrupted(), interrupted(): signaling (later...) 
    ​- setName("myThread") 
    ​- setDaemon(true/false) 
    ​- yield() - suggestion to thread scheduler, may be no-op 
    ​- setPriority(n): don't rely on it, OS priorities will be less
    fine-grained than Java's 
    ​- destroy(), stop() - don't call (later...) 

@page title@ ​ThreadGroup 
    ​- a group of Threads  
    ​- may contain other ThreadGroups 
    ​- common settings like: 
        ​- priority 
        ​- uncaught exception handler 
            ​(may be set on Thread, instance, ThreadGroup, Thread class) 
        ​- daemon 

@page title@ ​synchronized 
    ●Uses the object’s monitor 
    ●Provides mutual exclusion (mutex) for atomicity 
    ●Ordering: ‘leaving a synchronized block (releasing the monitor)
    happens-before every subsequent entry into blocks synchronised on
    the same object (obtaining the monitor)’. 
    ●Monitor is temporarily released, then re-acquired while wait()-ing
    – but only the monitor of the object wait() was invoked on! Don’t
    invoke wait() while holding other monitors, too! (Thead.sleep() does
    not release any locks.) 

@page title@ ​synchronized(cont’d) 
    ●Conditional wait(): wrap in while, not if (may return prematurely;
    other objects may use instances of your class in synchronized blocks
    and call wait()/notify[All]()). 
    ●Use notifyAll() instead of notify(), especially if multiple threads
    may be waiting on different conditions (one of them may be able to
    proceed, but if you only wake up one, it will consume the
    notification even if it cannot go on).  
    ●InterruptedException:  provides a nice way to quit – more on this
    later. 
@@@ code sample  @@@
    ​synchronized(theObject) { 
    ​        while(!done) { 
    ​                try { 
    ​                        theObject.wait(); 
    ​                } catch (InterruptedException e){ 
    ​                        // may exit here 
    ​                } 
    ​        } 
    ​} 

@@@ code sample @@@
    ​synchronized(theObject){ 
        done = true; 
    ​        theObject.notifyAll(); 
    ​} 

@page title@ ​synchronized(cont’d) – more warnings 

  *

    ●People using your class may call wait() and notify[All]() on
    instances for their own purposes, so don’t use this for
    synchronisation internally (synchronized methods and blocks, wait(),
    notify[All]()). Use a private final field instead. 

  *

    ●For the same reason, don’t synchronise on other people’s objects
    without consideration – they may synchronise on it internally. 

  *

    ●If possible, document how you handle synchronisation, and whether
    you want to co-operate with external users via synchronising on this. 

  *

    ​<number> 

 

  *

    ​<number> 

  *

    ​volatile 

  *

    ●Ordering (visibility): ‘A write to a volatile field happens before
    every subsequent read of that same field.’ 

      o

        ●Demo: mt.volatiledemo.BooleanDemo 

  *

    ●Atomicity of 64-bit assignment 

      o

        ●Demo: mt.volatiledemo.LongDemo 

  *

    ●Watch out for seemingly atomic operations – volatile won’t help you
    there! 

      o

        ●Demo: mt.syncdemo.PlusPlusDemo 

 

  *

    ​<number> 

  *

    ​final 

  *

    •Value cannot be changed. 

  *

    •Visibility: values of final fields are guaranteed to be visible to
    other threads when the constructor is finished, without any
    additional synchronisation. Note: this guarantee does not apply to
    non-final fields (synchronisation is needed, e.g. using the
    volatilekeyword)! 

 

  *

    ​<number> 

 

 

 

 

 

  *

    ​<number> 

  *

    ​Common problems and antipatterns 

  *

    •Broken synchronisation 

  *

    •Deadlocks 

  *
      o

        –The trivial: bad order 

      o

        –The tricky: sleeping/waiting with lock held 

  *

    •Race conditions 

  *

    •Performance bottlenecks 

  *

    •Unsafe publication 

  *

    •Threadsubclass starting itself in constructor 

 

  *

    ​Broken synchronisation – synchronising on updated reference 

  *

    ​Don’tdo this: 

  *

    ​<number> 

  *

    ​public void setField(Value newValue) { 

  *

    ​        synchronized(myField) { 

  *

    ​                myField = newValue; 

  *

    ​        } 

  *

    ​} 

  *

    ​public Value getField() { 

  *

    ​        synchronized(myField) { 

  *

    ​                return myField; 

  *

    ​        } 

  *

    ​} 

  *

    ​You are synchronising not on referring fields (those names are just
    symbols) but on referred objects! Synchronising on objects that
    might be replaced is usually pointless. 

 

  *

    ​Broken synchronisation – bad attempt at atomic operation 

  *

    ​Remember, lock is released in wait() 

  *

    ​<number> 

  *

    ​public class FortyTwo { 

  *

    ​        private int myX = 0; 

  *

    ​        myY = 42; 

  *

    ​        public synchronized void setInvariant(int value) { 

  *

    ​                myX = value; 

  *

    ​                try { 

  *

    ​                        this.wait(1000); 

  *

    ​                } catch (InterruptedException ignored) { 

  *

    ​                        // ignore exception 

  *

    ​                } 

  *

    ​                myY = 42 – value; 

  *

    ​        } 

  *

    ​        public synchronized int getInvariant() { 

  *

    ​                return myX + myY; 

  *

    ​        } 

  *

    ​} 

 

  *

    ​Race condition due to broken synchronisation attempt 

  *

    ​<number> 

 

  *

    ​<number> 

  *

    ​Deadlocks – circular waiting 

  *

    •Demo: mt.deadlock.Philosopher 

  *

    •Demo: mt.deadlock.SleepingWithLockHeld 

 

 

 

 

 

  *

    ​Race conditions 

  *

    •Non-deterministic behaviour caused by improper synchronisation 

  *

    •Outcome depends on timing of events 

  *

    •One thread not seeing an update or notification by another thread
    (e.g. it enters wait()only afterthe other thread calls notify()) 

  *

    •PlusPlusDemois an example of a race condition 

  *

    ​50 

 

  *

    ​Double-checked locking – the case of the broken singleton 

  *

    ​This won’twork: 

  *

    ​50 

  *

    ​public class Resource { 

  *

    ​        private static Resource ourInstance; 

  *

    ​        // some private instance fields go here, omitted from listing 

  *

    ​        private Resource() {…sets private fields…} 

  *

    ​        public static Resource get() { 

  *

    ​                // avoid synch overhead if instance is already there 

  *

    ​                if (null == ourInstance) { 

  *

    ​                        // doesn’t exist, create in thread-safe way 

  *

    ​                        synchronized(Resource.class) { 

  *

    ​                                // may have been created by another
    thread 

  *

    ​                                // after the non-synch’d check 

  *

    ​                                if (null == ourInstance) { 

  *

    ​                                        ourInstance = new Resource(); 

  *

    ​                                } 

  *

    ​                        }                         

  *

    ​                } 

  *

    ​                return ourInstance; 

  *

    ​        } 

  *

    ​} 

 

  *

    ​Performance bottlenecks 

  *

    •Synchronisation has costs 

  *

    •Obtaining a lock takes time 

  *
      o

        –Not really a problem unless the lock/volatile variable is
        heavily contested 

  *

    •Holding locks for too long blocks other threads 

  *

    ​50 

 

  *

    ​Perfomance bottlenecks (cont’d) 

  *

    •Do not call unknown methods while holding a lock. It may lead to
    performance bottlenecks and even deadlocks (if the called code locks
    some other object(s)). 

  *

    •Do not call Thread.sleep() while holding a lock (won’t be released). 

  *

    •Do not call Object.wait() while holding multiple locks (only the
    one of the waited-on object will be released). 

  *

    ​50 

 

  *

    ​Perfomance bottlenecks (cont’d) 

  *

    •Do not be overly strict. E.g. when accessing a shared object,
    multiple readers may be allowed, but only one writer. By simply
    making reader and writer methods synchronized, you won’t allow
    parallel reads! (We’ll get back to this issue later.) 

  *

    •Prefer synchronizedblocks over synchronizedmethods (finer
    granularity). 

  *

    ​50 

 

  *

    ​50 

  *

    ​Unsafe publication – thisescapes during construction 

  *

    ​public class Listener { 

  *

    ​        private String 

  *

    ​        myName; 

  *

    ​        Listener 

  *

    ​        (EventManager em){ 

  *

    ​                myName = "bad"; 

  *

    ​                em.subscribe(this); 

  *

    ​        } 

  *

    ​} 

  *

    ​public class ThreadUser { 

  *

    ​        private String myName; 

  *

    ​        ThreadUser() { 

  *

    ​                myName = "not good"; 

  *

    ​                (new Thread() { 

  *

    ​                        public void run() { 

  *

    ​                        ThreadUser.this. 

  *

    ​                                        doInThread(); 

  *

    ​                }}).start(); 

  *

    ​        } 

  *

    ​        private void doInThread() { 

  *

    ​                // do something 

  *

    ​        } 

  *

    ​} 

 

  *

    ​Unsafe vs. safe publication 

  *

    ●Immutable objects (all fields final) are safely published 

      o

        ●When you need to set multiple properties atomically, you should
        consider wrapping them in an immutable object. (The garbage
        collector is very good at handling small temporary objects.) 

      o

        ●See InvariantDemo. 

  *

    ●Otherwise, you need to publish the object via a volatile field,
    from a static initialiser, final field of a properly constructed
    object (this does not escape during construction), thread-safe
    collection or other thread-safe library classes (AtomicReference,
    BlockingQueue etc. – will talk about them later), or via a field
    guarded by a lock. 

  *

    ●Safe publication guarantees that other threads will see the object
    as published (if the object is published after the constructor
    returns, it will have been properly constructed). 

  *

    ●No guarantee on visibility of changes made after publication! 

  *

    ​50 

 

  *

    ​Threadsubclass starting itself in constructor 

  *

    ​50 

  *

    ​Class Service extends Thread{ 

  *

    ​        public Service() { 

  *

    ​                super(); 

  *

    ​                start(); 

  *

    ​        } 

  *

    ​        public void run() { 

  *

    ​                doService(); 

  *

    ​        } 

  *

    ​} 

  *

    ​Class CustomService 

  *

    ​        extends Service{ 

  *

    ​        private Thing myThing; 

  *

    ​        public CustomService 

  *

    ​        Thing t) { 

  *

    ​                super(); 

  *

    ​                myThing = t; 

  *

    ​                start(); 

  *

    ​        } 

  *

    ​        public void run(){                               
    super.doService(); 

  *

    ​                doMore(myThing); 

  *

    ​        } 

  *

    ​} 

 

  *

    ​50 

 

 

 

 

 

  *

    ​Thread termination 

  *

    ●Thread terminates when the run() method exits 

  *

    ●Do not call destroy() or stop() – they are deprecated. destroy()
    does not release locks (so no other thread can obtain them); stop()
    releases them abruptly (so the objects they guard may be left
    inconsistent) 

  *

    ●Use an exit condition (make sure changes are visible). 

  *

    ●Or interrupt the thread. 

  *

    ●Or send a poison pill. 

  *

    ●Some blocking operations require tricks. 

  *

    ​50 

 

  *

    ​Thread termination – exit condition 

  *

    ​class T extends Thread { 

  *

    ​        // must be volatile! 

  *

    ​        private volatile boolean myKeepRunning = true; 

  *

    ​        public void run() { 

  *

    ​                while(myKeepRunning) { 

  *

    ​                        doSomething(); 

  *

    ​                } 

  *

    ​        } 

  *

    ​        // don’t call this method stop() 

  *

    ​        // – that’s a deprecated method in Thread 

  *

    ​        public void shutdown() { 

  *

    ​                myKeepRunning = false; 

  *

    ​        } 

  *

    ​} 

  *

    ​50 

 

  *

    ​Thread termination – the ‘interrupted’ status 

  *

    •Each thread has an ‘interrupted’ flag 

  *

    •It is raised when aThread.interrupt()is called 

  *

    •It is cleared when InterruptedExceptionis thrown 

  *

    •It is clearedby Thread.interrupted() 

  *

    •It is not clearedby aThread.isInterrupted() 

  *

    ​50 

 

  *

    ​Thread termination – interrupting a passive thread 

  *

    ​public Result blockingOperation() { 

  *

    ​        Result r; 

  *

    ​        synchronized(myStateGuard) { 

  *

    ​                try { 

  *

    ​                        while(conditionIsNotMet()) { 

  *

    ​                                myStateGuard.wait(); 

  *

    ​                        } 

  *

    ​                        r = doSomething(); 

  *

    ​                } catch (InterruptedException e) { 

  *

    ​                        r = Result.ABORTED; 

  *

    ​                        // instead of special result, you could
    propagate 

  *

    ​                        // interruption info: call
    Thread.currentThread.interrupt()
                            // or don’t catch the exception 

  *

    ​                } 

  *

    ​        } 

  *

    ​        return result; 

  *

    ​} 

  *

    ​50 

 

  *

    ​Thread termination – interrupting a busy thread 

  *

    ​50 

  *

    ​void blockingOperation() { 

  *

    ​        while(!Thread.interrupted()) { 

  *

    ​                doSomething(); 

  *

    ​        } 

  *

    ​} 

  *

    ​void blockingOperation() { 

  *

    ​        while(!Thread.currentThread().isInterrupted()) { 

  *

    ​                doSomething(); 

  *

    ​        } 

  *

    ​} 

 

  *

    ​Thread termination – propagating the ‘interrupted’ status 

  *

    ●Don’t catch the exception, caller will know about interrupt – only
    if you can exit immediately. 

  *

    ●Finish processing, return special result. 

  *

    ●Interrupt the thread again to raise the flag, return normal result.
    Blocking methods in caller will detect interruption, but be careful
    with infinite loops (if caller calls your method in a loop, it’ll be
    you who detects the interrupt again…). 

  *

    ​50 

 

  *

    ​Thread termination – propagating the ‘interrupted’ status 

  *

    ​public Result mustComplete() { 

  *

    ​        boolean wasInterrupted = false; 

  *

    ​        synchronized(myStateGuard) { 

  *

    ​                while(! canProceed()) { 

  *

    ​                        try { 

  *

    ​                                myStateGuard.wait(); 

  *

    ​                        } catch (InterruptedException e) { 

  *

    ​                                // note the interruption, but carry on 

  *

    ​                                wasInterrupted = true; 

  *

    ​                        } 

  *

    ​                } 

  *

    ​        } 

  *

    ​        Result r = calculateResult(); 

  *

    ​        if (wasInterrupted) { 

  *

    ​                // let caller know 

  *

    ​                Thread.currentThread().interrupt(); 

  *

    ​        } 

  *

    ​        return r; 

  *

    ​} 

  *

    ​50 

 

  *

    ​Thread termination - The poison pill 

  *

    ​void serviceRequests(Queue<Request> q) { 

  *

    ​        while(true) { 

  *

    ​                Request r = q.remove(); 

  *

    ​                // PoisonPill extends Request 

  *

    ​                // it has a single, final instance 

  *

    ​                if (r != PoisonPill.instance) { 

  *

    ​                        processRequest(r); 

  *

    ​                } else { 

  *

    ​                        break; 

  *

    ​        } 

  *

    ​} 

  *

    ​50 

 

  *

    ​Thread termination - Tricky shutdowns 

  *

    Synchronous socket I/O (java.io): closing the Input/OutputStream
    will cause SocketException in blocked thread 

  *

    Synchronous I/O (java.nio): close the channel. InterruptibleChannel
    is better, interrupting one thread closes the channel and all
    threads throw ClosedByInterruptException. 

  *

    Asynchronous I/O with Selector (java.nio.channels):  wakeup() will
    unblock threads waiting in Selector.select(), throwing
    ClosedSelectorException 

  *

    Waiting for monitor entry: 

  *
      o

        make sure it can acquire the lock, or 

      o

        use Lock.lockInterruptibly()(more on this later) 

  *

    ​50 

 

  *

    ​50 

 

 

  *

    ​Synchronised collections 

  *

    ●Synchronized collections 

  *

    ●Some collection classes are synchronised (e.g. Vector is
    synchronised, ArrayList is not) 

  *

    ●Collections.synchronizedXyz() wraps existing collections to make
    retrieval, modification and toString() methods synchronized. 

  *

    ●Demo: mt.syncdemo.ConcurrentEventVenue1 

  *

    ​50 

 

  *

    ​Synchronized collections – when they fail 

  *

    ●Demo: mt.syncdemo.ConcurrentEventVenue2 

  *

    ●Iterators on standard collections are fail-fast: they attempt to
    detect modification of the underlying collection. 

  *

    ●Such hidden iterators can be very hard do diagnose. (Just for fun:
    try st.CmeDemo) 

  *

    ●Plan synchronisation carefully. 

  *

    ●Concurrent collections may help. (Will talk about them later.) 

  *

    ​50 

 

  *

    ​java.lang.ThreadLocal 

  *

    ●ThreadLocal 

  *

    ●Normally, fields are bound to an object (instance or class):
    several threads accessing the same field of the same container
    object will access the same reference. 

  *

    ●Each thread will see a thread-specific value in the same
    ThreadLocal field. 

  *

    ●Picture it as a Map, implicitly keyed by Thread instances. 

  *

    ●Generic class. 

  *

    ●Demo: mt.threadlocal.ThreadLocalDemo 

  *

    ​50 

 

  *

    ​50 

 

 

  *

    ​Queues – java.util.Queue 

  *

    ​Queues 

  *

    •For consumer/producer setups 

  *

    •Typically FIFO, but could be LIFO (stack) or priority-based 

  *

    •Head and tail 

  *

    •Extends java.util.Collection 

  *

    •Operations for: 

  *
      o

        –storing elements: add() / offer() 

      o

        –removing elements: remove() / poll() 

      o

        –examining: element() / peek() 

  *

    ​50 

 

  *

    ​Queues – some general-purpose types 

  *

    •BlockingQueue: blocks on addition when capacity reached / removal
    if empty (may be bounded or unbounded). Shutdown support:
    drainTo(Collection c). Demo: mt.queues.ProducerConsumerDemo 

  *

    •Deque(‘deck’): double-ended, may insert at /remove from both ends
    (could be used as a LIFO (stack)) 

  *

    •BlockingDeque 

  *

    ​50 

 

  *

    ​Queues – some special-purpose types 

  *

    •PriorityQueue: returns elements in priority-based order (not
    insertion order) 

  *

    •DelayQueue: each element is associated with a delay; elements are
    returned only after delay elapses, in the order their delay elapsed
    (demo: mt.queues.DelayQueueDemo) 

  *

    •SynchronousQueue: to implement rendezvous (no capacity, reader and
    writer block until the other party arrives) 

  *

    ​50 

 

  *

    ​Concurrent collections – general characteristics 

  *

    ​Concurrent collections 

  *

    •Sometimes it is not important if we miss an update when iterating
    over data (will see it the next time) 

  *

    •Some classes provide much better performance than simple
    synchronized version due to special synchronisation mechanisms (e.g.
    only parts of the collection are locked, allowing concurrent reads
    and updates on other parts of the collection) 

  *

    •Fail-safe iterators 

  *

    ​50 

 

  *

    ​Concurrent collections – quick overview 

  *

    •List: CopyOnWriteArrayList: best for rarely written, often read
    collections (list of event listeners etc.). A new copy of content is
    made on each modification, iterator will use the old copy. 

  *

    •Sets: CopyOnWriteArraySet (a new copy is made on each
    modification), ConcurrentSkipListSet (NavigableSet) 

  *

    •Maps: ConcurrentHashMap (very good performance),
    ConcurrentSkipListMap (NavigableMap). Atomic operations of
    ConcurrentMap: putIfAbsent, remove, replace 

  *

    •Demo: Fixing mt.syncdemo.ConcurrentEventVenue2 

  *

    ​50 

 

  *

    ​Atomic operations 

  *

    ​Atomic operations 

  *

    •Many programs need check-then-modify operations 

  *

    •Examples are: 

  *
      o

        –incrementing counters (remember PlusPlusDemo?) 

      o

        –set some value if current value matches a desired value (e.g.
        is null – a kind of locking can be achieved by storing a
        reference to an ‘owner’ (object or thread), and checking/setting
        this owner) 

      o

        –create object if does not exist 

  *

    •Traditionally, all those operations require locking, which has
    overhead, and may be forgotten. 

  *

    •Modern CPUs support atomic check-and-set operations, making
    lock-free atomic operationspossible. 

  *

    ​50 

 

  *

    ​Atomic reference types 

  *

    •AtomicReference (+Array): general purpose (compareAndSet, getAndSet) 

  *

    •AtomicBoolean: for flags 

  *

    •AtomicInteger/Long (+Array types): numbers (e.g. counters). Ops: 

  *
      o

        –addAndGet / getAndAdd 

      o

        –incrementAndGet / getAndIncrement 

      o

        –decrementAndget / getAndDecrement 

  *

    •AtomicReference/Integer/LongFieldUpdater: reflection-based updaters
    for volatile fields (you can add atomic operations to your own classes) 

  *

    •AtomicMarkableReference: value + boolean marker that can be updated
    atomically (e.g. boolean could indicate deleted status) 

  *

    •AtomicStampedReference: value + integer (e.g. version number) 

  *

    ​50 

 

  *

    ​Atomic reference types - demo 

  *

    •Try fixed (synchronized) version of PlusPlusDemo: mt.syncdemo.
    SynchronizedPlusPlusDemo 

  *

    •Now try the atomic version, and compare performance:
    mt.syncdemo.AtomicPlusPlusDemo 

  *

    ​50 

 

  *

    ​50 

 

 

  *

    ​Other synchronisation facilities – Semaphoreand CountDownLatch 

  *

    •Semaphore: allows threads to obtain ‘permits’. Shutdown support:
    drainPermits(). Both interruptible and uninterruptible modes supported. 

  *

    •CountDownLatch: initialised with a number, can only be decremented
    (countDown()). Threads may call await()to wait for counter to reach 0. 

  *

    •Demo: check use of semaphore in AtomicPlusPlusDemo, replace with
    latch. 

  *

    •Demo: mt.syncfacilitiesdemo.ServiceStation 

  *

    ​50 

 

  *

    ​Other synchronisation facilities – CyclicBarrier 

  *

    •May be used to get all co-operating worker threads wait for each-other 

  *

    •Has an optional Runnable target which is invoked when all workers
    are done, to merge/post-process individual results. 

  *

    ​50 

 

  *

    ​50 

 

 

  *

    ​Locks 

  *

    ​synchronizedblocks are simple, but have drawbacks: 

  *

    •blocked threads cannot be interrupted 

  *

    •cannot differentiate between reader and writer locks, which may
    lead to performance bottlenecks 

  *

    ​50 

 

  *

    ​Implicit vs. explicit locks 

  *

    ●Despite their drawbacks, implicit locks (synchronized blocks) are
    usually preferrable. They are simple (it’s easy to see the beginning
    and end of block) and are safe (locks are released when you leave
    the block for any reason, including exceptions). 

  *

    ●With explicit locks, it may not be easy to keep track of locking /
    unlocking, especially if they are in different parts of the code.
    Exception handling can become a nightmare. 

  *

    ​50 

 

  *

    ​Ready-to-use explicit locks 

  *

    ●ReentrantLock – provides the same functionality as synchronized
    blocks, but more flexible (interruptible and time-limited lock
    attempts, lock only if currently not locked). Condition objects
    provide wait()-like functionality. 

  *

    ●ReentrantReadWriteLock – supports shared reads, exclusive writes. 

  *

    ●Custom synchronisers may be developed using some supporting classes
    of java.util.concurrent.locks. 

  *

    ●Demo: mt.locks.ReadWriteDemo 

  *

    ​50 

 

  *

    ​50 

 

 

  *

    ​The Executor framework 

  *

    •A set of classes and interfaces with advanced taskexecuting services. 

  *

    •Flexible thread-usage strategies (thread pools) 

  *

    •Possible to impose time limit on tasks 

  *

    •Possible to cancel execution 

  *

    •Complex topic, not covered here, but mentioned so you know it is
    there. 

  *

    ​50 

 

  *

    ​The Executor framework – a simple example 

  *

    ​class TaskExecutionWebServer { 

  *

    ​        private static final int NTHREADS = 100; 

  *

    ​        private static final Executor exec =                      
                                             
    Executors.newFixedThreadPool(NTHREADS); 

  *

    ​        public static void main(String[] args) throws IOException { 

  *

    ​                ServerSocket socket = new ServerSocket(80); 

  *

    ​                while (true) { 

  *

    ​                        final Socket connection = socket.accept(); 

  *

    ​                        Runnable task = new Runnable() { 

  *

    ​                                public void run() { 

  *

    ​                                        handleRequest(connection); 

  *

    ​                                } 

  *

    ​                        }; 

  *

    ​                        exec.execute(task); 

  *

    ​                } 

  *

    ​        } 

  *

    ​ }  

  *

    ​50 

 

  *

    ​The Exectutor framework – thread pools (ExecutorService) 

  *

    •Create using convenience factory methods on the Executorsclass or
    by instantiating ExecutorServiceimplementors (more flexible, may
    provide work queue etc.) 

  *

    •Available thread pools: 

  *
      o

        –Fixed: fixed number of threads 

      o

        –Cached: dynamically changing pool size, thread reuse,
        termination of unused threads, on-demand creation of new threads 

      o

        –Scheduled: for executing tasks after a delay or periodically 

      o

        –SingleThreaded: a single thread 

  *

    ​50 

 

  *

    ​The Executor framework – Callableand Future 

  *

    •Callable<T>: represents a result-bearing task: 

  *
      o

        ​T call(); 

  *

    •Future<T>: represents the result of a task, provides detection of
    completion, cancellation, interruption 

  *
      o

        ​boolean cancel(boolean mayInterruptIfRunning); 

      o

        ​boolean isCancelled(); 

      o

        ​isDone(); 

      o

        ​T get(); 

      o

        ​T get(long timeout, TimeUnit unit); 

  *

    ​50 

 

  *

    ​The Executor framework – shutting down 

  *

    •Life cycle: running, shutting down, terminated 

  *

    •Life cycle methods of ExecutorService(thread pool): 

  *
      o

        ​void shutdown(); 

      o

        ​List<Runnable> shutdownNow(); 

      o

        ​boolean isShutdown(); 

      o

        ​boolean isTerminated(); 

      o

        ​boolean awaitTermination(long timeout, 

      o

        ​                TimeUnit unit) throws                          
                                      InterruptedException; 

  *

    •Tasks submitted after shutdown are rejected. Rejection policy is
    configurable.  

  *

    ​50 

 

  *

    ​The Executor framework – Rejection policy (RejectedExecutionHandler) 

  *

    •The rejection policy applies when: 

  *
      o

        –the ExecutorServicehas been (asked to) shut down 

      o

        –the work queue has limited capacity and limit has been reached 

  *

    •Provided policies: 

  *
      o

        –AbortPolicy: RejectedExecutionException (unchecked) is thrown 

      o

        –CallerRunsPolicy: the task is executed in the caller’s thread,
        effectively preventing it from submitting more tasks (graceful
        degradation) 

      o

        –DiscardPolicy: the newly submitted task is discarded 

      o

        –DiscardOldestPolicy: discard the task which would be executed
        next, then resubmit (not a good idea if the work queue is a
        PriorityQueue) 

  *

    ​50 

 

  *

    ​The Executor framework – wrap-up 

  *

    •Versatile and powerful framework 

  *

    •Quite complex 

  *

    •Could be very useful as building blocks 

  *

    •For more info, check out the API docs and Java Concurrency in
    Practice, which discusses the framework in detail and shows some
    useful classes built on top of it. 

  *

    ​50 

 

  *

    ​The End… 

  *

    ​Thank you! 

  *

    ​50 

 

 

  *

    ​Delivering Excellence in Software Engineering  

 
