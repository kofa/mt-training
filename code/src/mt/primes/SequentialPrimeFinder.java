package mt.primes;

import java.util.ArrayList;
import java.util.List;

public class SequentialPrimeFinder implements PrimeFinder {
	
	public List<Long> findPrimes(long from, long to) {
		List<Long> primes = new ArrayList<>();
		for (long i = from; i < to; i++) {
			if (isPrime(i)) {
				primes.add(i);
			}
		}
		return primes;
	}
	
	private boolean isPrime(long i) {
		return (i == 2) || (((i & 1) != 0) && checkPrime(i)); 
	}

	private boolean checkPrime(long i) {
		long maxFactor = (long) Math.sqrt(i);
		boolean mayBePrime = true;
		long divisor = 3;
		while (mayBePrime && divisor <= maxFactor) {
			mayBePrime = (i % divisor != 0);
			divisor++;
		}
		return mayBePrime;
	}

	public static void main(String[] args) {
		test(new SequentialPrimeFinder());
		test(new ForkJoinPrimeFinder(100_000));
	}
	
	private static void test(PrimeFinder finder) {
		long start = System.currentTimeMillis();
		List<Long> primes = finder.findPrimes(2, 4_000_000);
		long end = System.currentTimeMillis();
		long duration = end - start;
		System.out.println("Found " + primes.size() + " primes in " + duration + "ms");
		//System.out.println(primes);
	}
}
