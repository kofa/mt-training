package mt.primes;

import java.util.List;

public interface PrimeFinder {
	List<Long> findPrimes(long from, long to);
}
