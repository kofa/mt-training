package mt.sum;

public interface SumFinder {
    long sum(int[] data);
    void shutdown();
}
