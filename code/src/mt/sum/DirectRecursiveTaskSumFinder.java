package mt.sum;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;
import java.util.concurrent.TimeUnit;

public class DirectRecursiveTaskSumFinder implements SumFinder {
    private final ForkJoinPool pool = new ForkJoinPool();
    private final int sequentialThreshold;

    public DirectRecursiveTaskSumFinder(int sequentialThreshold) {
        this.sequentialThreshold = sequentialThreshold;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + '/' + sequentialThreshold;
    }

    @Override
    public long sum(int[] data) {
        SumFinderTask sumFinder = new SumFinderTask(data, 0, data.length);
        return sumFinder.compute();
    }

    @Override
    public void shutdown() {
        pool.shutdown();
        boolean shutDown;
        try {
            shutDown = pool.awaitTermination(1, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            throw new RuntimeException("Shutdown failed", e);
        }
        if (!shutDown) {
            throw new RuntimeException("Shutdown failed");
        }
    }

    private class SumFinderTask extends RecursiveTask<Long> {
        private int[] data;
        private int fromIndex;
        private int toIndex;

        SumFinderTask(int[] data, int fromIndex, int toIndex) {
            this.data = data;
            this.fromIndex = fromIndex;
            this.toIndex = toIndex;
        }

        @Override
        protected Long compute() {
            long sum;
            int size = toIndex - fromIndex;
            if (size < sequentialThreshold) {
                sum = SummationLoop.sum(data, fromIndex, toIndex);
            } else {
                int rightSideFromIndex = fromIndex + (size / 2);
                SumFinderTask left = new SumFinderTask(data, fromIndex, rightSideFromIndex);
                SumFinderTask right = new SumFinderTask(data, rightSideFromIndex, toIndex);
                left.fork();
                sum = right.compute() + left.join();
            }
            return sum;
        }
    }
}