package mt.sum;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.TimeUnit;

public class ForkingRecursiveActionSumFinder implements SumFinder {
    private final ForkJoinPool pool = new ForkJoinPool();
    private final int sequentialThreshold;

    public ForkingRecursiveActionSumFinder(int sequentialThreshold) {
        this.sequentialThreshold = sequentialThreshold;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + '/' + sequentialThreshold;
    }

    @Override
    public long sum(int[] data) {
        SumFinderAction sumFinder = new SumFinderAction(data, 0, data.length);
        sumFinder.compute();
        return sumFinder.sum;
    }

    @Override
    public void shutdown() {
        pool.shutdown();
        boolean shutDown;
        try {
            shutDown = pool.awaitTermination(1, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            throw new RuntimeException("Shutdown failed", e);
        }
        if (!shutDown) {
            throw new RuntimeException("Shutdown failed");
        }
    }

    private class SumFinderAction extends RecursiveAction {
        private long sum = 0;
        private int[] data;
        private int fromIndex;
        private int toIndex;

        SumFinderAction(int[] data, int fromIndex, int toIndex) {
            this.data = data;
            this.fromIndex = fromIndex;
            this.toIndex = toIndex;
        }

        @Override
        protected void compute() {
            int size = toIndex - fromIndex;
            if (size < sequentialThreshold) {
                sum = SummationLoop.sum(data, fromIndex, toIndex);
            } else {
                int rightSideFromIndex = fromIndex + (size / 2);
                SumFinderAction left = new SumFinderAction(data, fromIndex, rightSideFromIndex);
                SumFinderAction right = new SumFinderAction(data, rightSideFromIndex, toIndex);
                invokeAll(left, right);
                sum = left.sum + right.sum;
            }
        }
    }
}