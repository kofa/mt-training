package mt.sum;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;
import java.util.concurrent.TimeUnit;

public class ForkingRecursiveTaskSumFinder implements SumFinder {
    private final ForkJoinPool pool = new ForkJoinPool();
    private final int sequentialThreshold;

    public ForkingRecursiveTaskSumFinder(int sequentialThreshold) {
        this.sequentialThreshold = sequentialThreshold;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + '/' + sequentialThreshold;
    }

    @Override
    public long sum(int[] data) {
        pool.getPoolSize();
        SumFinderTask sumFinder = new SumFinderTask(data, 0, data.length);
        return sumFinder.compute();
    }

    @Override
    public void shutdown() {
        pool.shutdown();
        boolean shutDown;
        try {
            shutDown = pool.awaitTermination(1, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            throw new RuntimeException("Shutdown failed", e);
        }
        if (!shutDown) {
            throw new RuntimeException("Shutdown failed");
        }
    }

    private class SumFinderTask extends RecursiveTask<Long> {
        private final int size;
        private int[] data;
        private int fromIndex;
        private int toIndex;

        SumFinderTask(int[] data, int fromIndex, int toIndex) {
            this.data = data;
            this.fromIndex = fromIndex;
            this.toIndex = toIndex;
            this.size = toIndex - fromIndex;
        }

        @Override
        protected Long compute() {
            long sum = 0;
            log(this + " -> compute() invoked");
            if (size < sequentialThreshold) {
                sum = SummationLoop.sum(data, fromIndex, toIndex);
            } else {
                int rightSideFromIndex = fromIndex + (size / 2);
                SumFinderTask left = new SumFinderTask(data, fromIndex, rightSideFromIndex);
                SumFinderTask right = new SumFinderTask(data, rightSideFromIndex, toIndex);
                log(this + " invoking " + left + " and " + right);
                invokeAll(left, right);
////                left.fork();
////                right.fork();
//                ForkJoinTask<Long> left = new SumFinderTask(data, fromIndex, rightSideFromIndex).fork();
//                ForkJoinTask<Long> right = new SumFinderTask(data, rightSideFromIndex, toIndex).fork();
                log(this + " waiting to join " + left + " and " + right);
                sum = left.join() + right.join();
            }
            return sum;
        }

        @Override
        public String toString() {
            return this.getClass().getSimpleName() + "[" + fromIndex + ", " + toIndex + "), size: " + size;
        }
    }
    private void log(String message) {
        //System.out.println(System.currentTimeMillis() + " " + Thread.currentThread().getName() + " " + message);
    }
}