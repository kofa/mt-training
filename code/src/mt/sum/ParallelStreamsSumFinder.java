package mt.sum;

import java.util.Arrays;

public class ParallelStreamsSumFinder implements SumFinder {
    @Override
    public long sum(int[] data) {
//        return Arrays.stream(data).parallel().reduce(Integer.MIN_VALUE, Integer::sum);
        return Arrays.stream(data).asLongStream().parallel().sum();
        //return Arrays.stream(data).parallel().sum().getAsInt();
    }

    @Override
    public void shutdown() {
        // nothing to do here
    }
}
