package mt.box;

import java.util.concurrent.atomic.AtomicInteger;

public class Box {
    public static final int N_LOOPS = 100;
    private boolean occupied;
    private final Object lock = new Object();
    private final AtomicInteger runningParties = new AtomicInteger(0);

    private void markRunningAndWaitForOtherParty() {
        runningParties.incrementAndGet();
        while (runningParties.get() != 2)
            ;
    }

    private void producer() {
        markRunningAndWaitForOtherParty();
        for (int i = 0; i < N_LOOPS; i++) {
            synchronized (lock) {
                waitUntilFree();
                fillTheBox();
                wakeCounterParty();
            }
        }
    }

    private void wakeCounterParty() {
        lock.notifyAll();
    }

    private void consumer() {
        markRunningAndWaitForOtherParty();
        for (int i = 0; i < N_LOOPS; i++) {
            synchronized (lock) {
                waitUntilNotEmpty();
                emptyTheBox();
                wakeCounterParty();
            }
        }
    }

    private void waitUntilNotEmpty() {
        while (!occupied) {
            waitOnLock();
        }
    }

    private void waitOnLock() {
        try {
            lock.wait();
        } catch (InterruptedException e) {
            System.exit(1);
        }
    }

    private void emptyTheBox() {
        occupied = false;
    }

    private void fillTheBox() {
        occupied = true;
    }

    private void waitUntilFree() {
        while (occupied) {
            waitOnLock();
        }
    }

    private class Producer implements Runnable {
        @Override
        public void run() {
            producer();
            System.out.println("Producer done");
        }
    }

    private class Consumer implements Runnable {
        @Override
        public void run() {
            consumer();
            System.out.println("Consumer done");
        }
    }

    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 10000; i++) {
            new Box().run();
        }
    }

    private void run() throws InterruptedException {
        Thread consumer = new Thread(new Consumer(), "Consumer");
        Thread producer = new Thread(new Producer(), "Producer");
        consumer.start();
        producer.start();
        consumer.join();
        producer.join();
    }
}
